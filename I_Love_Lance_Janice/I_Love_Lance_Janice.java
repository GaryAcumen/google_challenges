package com.google.challenges;
import java.util.*;
import java.lang.*;

public class Answer {
	public static String answer(String s) {
		Map<String, String> letterMap = buildLetterMap();
		String myAnswer = "";
		for (Character c: s.toCharArray()) {
			if (letterMap.containsKey(c.toString())) {
				myAnswer += letterMap.get(c.toString());
			} else {
				myAnswer += c;
			}
		}

		return myAnswer;
	}

	public static Map<String, String> buildLetterMap() {
		Map<String, String> oppositesMap = new HashMap<String,String>();
		oppositesMap.put("a", "z");
		oppositesMap.put("b", "y");
		oppositesMap.put("c", "x");
		oppositesMap.put("d", "w");
		oppositesMap.put("e", "v");
		oppositesMap.put("f", "u");
		oppositesMap.put("g", "t");
		oppositesMap.put("h", "s");
		oppositesMap.put("i", "r");
		oppositesMap.put("j", "q");
		oppositesMap.put("k", "p");
		oppositesMap.put("l", "o");
		oppositesMap.put("m", "n");
		oppositesMap.put("n", "m");
		oppositesMap.put("o", "l");
		oppositesMap.put("p", "k");
		oppositesMap.put("q", "j");
		oppositesMap.put("r", "i");
		oppositesMap.put("s", "h");
		oppositesMap.put("t", "g");
		oppositesMap.put("u", "f");
		oppositesMap.put("v", "e");
		oppositesMap.put("w", "d");
		oppositesMap.put("x", "c");
		oppositesMap.put("y", "b");
		oppositesMap.put("z", "a");
		return oppositesMap;
	}
}