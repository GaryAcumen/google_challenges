// package com.google.challenges;
import java.lang.Integer;
import java.util.Arrays;

public class Answer {
	public static int[] answer(int[] l, int t) {
		for (int start = 0; start <= l.length; start++) {
			int total = 0;
			for (int end = start+1; end <= l.length; end++) {
				total += l[end-1];
				if (total == t) {
					return new int[]{start, end-1};
				}
			}
		}

		return new int[]{-1, -1};
	}


	public static void main(String[] args) {
		int [] intArr = new int[args.length -1];

		for (int i = 0; i < intArr.length; i++) {
			intArr[i] = Integer.parseInt(args[i]);
		}

		int t = Integer.parseInt(args[args.length-1]);

		System.out.println(Arrays.toString(Answer.answer(intArr, t)));
	}
}