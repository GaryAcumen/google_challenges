import java.util.*;
import java.lang.*;

public class Answer{

	public static int[] answer(int[] data, int n) { 
		if (n == 0) {
			return new int[0];
		} else if (n > data.length) {
			return data;
		}

		return calculateArray(data, n);
	}

	public static int[] calculateArray(int[] data, int n) {
		Map<Integer, Integer> mapNums = new HashMap<Integer,Integer>();
		ArrayList<Integer> dataList = new ArrayList<Integer>();
		for (int i : data) {
			if (mapNums.containsKey(i)) {
				mapNums.put(i, mapNums.get(i)+1);
			} else {
				mapNums.put(i, 1);
			}
			dataList.add(i);
		}
		for (Integer i : mapNums.keySet()) {
			if (mapNums.get(i) > n) {
				ArrayList<Integer> iList = new ArrayList<Integer>();
				iList.add(i);
				dataList.removeAll(iList);
			}
		}
		int[] intArray = new int[dataList.size()];
		int cnt = 0;
		for (Integer i : dataList) {
			System.out.println(i);
			intArray[cnt] = i;
			cnt++;
		}
		return intArray;
	}
}