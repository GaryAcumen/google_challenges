package com.google.challenges;
import java.math.BigInteger;
import java.util.*;

public class Answer {

	static private boolean isPossible;

	/* 
	one is divisible by another and bigger than 1 => impossible
	recursive solution is similar to implementation of gcd
	Formula: F(a, b) = (a / b) + F(b, a mod b)
	*/
	public static BigInteger runCycles(BigInteger a, BigInteger b) {
		System.out.println("a: " + a.toString());
		System.out.println("b: " + b.toString());
		if (b.compareTo(BigInteger.valueOf(0)) == 0) {
			return a;
		}
		else if (a.mod(b) == BigInteger.valueOf(0) && b.compareTo(BigInteger.valueOf(1)) > 0) {
			isPossible = false;
		}
		return a.divide(b).add(runCycles(b, a.mod(b)));
	}

	public static String answer(String M, String F) {
		// Your code goes here.
		BigInteger m = new BigInteger(M);
		BigInteger f = new BigInteger(F);
		isPossible = true;
		System.out.println("m: " + m.toString());
		System.out.println("f: " + f.toString());
		// 2 is subtracted because we start from (0, 0)
		BigInteger res = runCycles(m, f).subtract(BigInteger.valueOf(2));
		if (!isPossible) {
			return "impossible";
		} else {
			return res.toString();
		}
	}

	// public static String answer(String M, String F) {
	// 	String s1 = "1";
	// 	String s2 = "2";
	// 	String s0 = "0";

	// 	BigInteger divisor = new BigInteger(s2);
	// 	BigInteger modInt = new BigInteger(s1);
	// 	BigInteger zero = new BigInteger(s0);

	// 	BigInteger m = new BigInteger(M);
	// 	BigInteger f = new BigInteger(F);

	// 	boolean doThisThing = true;
	// 	String returnStr = "";

	// 	int count = 0;

	// 	if (m.equals(modInt) && f.equals(modInt)) {
	// 		return "0";
	// 	} else if (m.equals(modInt) || f.equals(modInt)) {
	// 		if (m.compareTo(f) == 1) {
	// 			return m.subtract(modInt).toString();
	// 		} else if (f.compareTo(m) == 1) {
	// 			return f.subtract(modInt).toString();
	// 		}
	// 	}

	// 	// now lets reduce the operations needed.
	// 	// if f*2 is less than m you can reduce my division
	// 	// or vice versa
	// 	if (m.compareTo(f) == 1) {
	// 		if (f.multiply(divisor).compareTo(m) <= 0) {
	// 			count = m.divide(f).intValue();
	// 			BigInteger cnt = BigInteger.valueOf(count);
	// 			m = m.subtract(f.multiply(cnt));
	// 		}
	// 	} else if (f.compareTo(m) == 1) {
	// 		if (m.multiply(divisor).compareTo(f) <= 0) {
	// 			count = f.divide(m).intValue();
	// 			BigInteger cnt = BigInteger.valueOf(count);
	// 			f = f.subtract(m.multiply(cnt));
	// 		}
	// 	}

	// 	if (m.equals(modInt) || f.equals(modInt)) {
	// 		if (m.compareTo(f) == 1) {
	// 			count += m.subtract(modInt).intValue();
	// 			return Integer.toString(count);
	// 		} else if (f.compareTo(m) == 1) {
	// 			count += f.subtract(modInt).intValue();
	// 			return Integer.toString(count);
	// 		}
	// 	}

	// 	if ((!m.mod(divisor).equals(modInt) && !f.mod(divisor).equals(modInt)) || 
	// 				(m.equals(zero) && f.equals(zero)) || f.equals(m)) {
	// 		returnStr = "impossible";
	// 		return returnStr;
	// 	}

	// 	while(doThisThing) {
	// 		if ((m.compareTo(modInt) == 1 || f.compareTo(modInt) == 1) && m.compareTo(modInt) > -1 && f.compareTo(modInt) > -1) {
	// 			//this means that one of the numbers is greater than 1 and the other isn't less than 1
	// 			if (m.compareTo(f) == 1) {
	// 				m = m.subtract(f);
	// 				count++;
	// 			} else if (f.compareTo(m) == 1) {
	// 				f = f.subtract(m);
	// 				count++;
	// 			} else {
	// 				doThisThing = false;
	// 				returnStr = "impossible";
	// 			}
	// 		} else if (m.equals(modInt) && f.equals(modInt)) {
	// 			doThisThing = false;
	// 			returnStr = Integer.toString(count);
	// 		} else if (f.equals(m)) {
	// 			doThisThing = false;
	// 			returnStr = "impossible";
	// 		} else {
	// 			doThisThing = false;
	// 			returnStr = "impossible";
	// 		}
	// 	}
	// 	return returnStr;
	// }
	
}