import java.util.Arrays;

public class Answer3 {
	public static int answer(int[] l) {
		//assumes sorted array input
		int noOfCombinations = 0;
		int[] noOfDoubles = new int[l.length];

		// Count lucky doubles for each item in the array, except the first and last items
		for(int i = 1; i < l.length-1; i++) {
			for(int j = 0; j < i; j++) {
				if(l[i] % l[j] == 0)
					noOfDoubles[i]++;
			}
		}

		// Count lucky triples
		for(int i = 2; i < l.length; i++) {
			for(int j = 1; j < i; j++) {
				if(l[i] % l[j] == 0)
					noOfCombinations += noOfDoubles[j];
			}
		}

		return noOfCombinations;
	}

	public static void main(String[] args) {

		int[] x = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			x[i] = Integer.parseInt(args[i]);
		}

		System.out.println(Answer3.answer(x));
	}
}


//Use below for testing in HackerRank
/*
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {
	public static int answer(int[] l) {
		//assumes sorted array input
		int noOfCombinations = 0;
		int[] noOfDoubles = new int[l.length];

		// Count lucky doubles for each item in the array, except the first and last items
		for(int i = 1; i < l.length-1; i++) {
			for(int j = 0; j < i; j++) {
				if(l[i] % l[j] == 0)
					noOfDoubles[i]++;
			}
		}

		// Count lucky triples
		for(int i = 2; i < l.length; i++) {
			for(int j = 1; j < i; j++) {
				if(l[i] % l[j] == 0)
					noOfCombinations += noOfDoubles[j];
			}
		}

		return noOfCombinations;
	}

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s = in.next();
        String[] stringArr = s.split(",");
        int[] intArr = new int[stringArr.length];
        for (int i = 0; i < stringArr.length; i++) {
            intArr[i] = Integer.parseInt(stringArr[i]);
        }
        System.out.println(answer(intArr));
    }
}

 */