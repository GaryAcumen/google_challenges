import java.lang.*;
import java.util.*;
import java.math.*;

public class Answer {
	public static int answer(int[] l) {

		if (l.length < 3) {
			return 0;
		}

		//sort array
		//only keep unique values
		Set<Integer> uniqueSet = new HashSet<Integer>();
		for (int i : l) {
			uniqueSet.add(new Integer(i));
		}
		ArrayList<Integer> uniqueList = new ArrayList<Integer>(uniqueSet);
		Collections.sort(uniqueList);

		ArrayList<ArrayList<Integer>> allCombs = getCombs(uniqueList, 3);

		int total = 0;
		for (ArrayList<Integer> intArr : allCombs) {
			if (checkTriple(intArr)) {
				total++;
			}
		}

		return total;
	}

	public static ArrayList<ArrayList<Integer>> getCombs(ArrayList<Integer> originalList, int k) {
		ArrayList<ArrayList<Integer>> returnList = new ArrayList<ArrayList<Integer>>();
		//primitive cases
		if (k == 0) {
			//return empty list
			returnList.add(new ArrayList<Integer>());
			return returnList;
		}

		if (k > originalList.size()) {
			return returnList;
		}

		ArrayList<Integer> subList = new ArrayList<Integer>(originalList.subList(0, originalList.size() - 1));

		//first cond: same k w/out last element
		returnList.addAll(getCombs(subList, k));
		//second cond: k - 1 w/out last element with last element added
		ArrayList<ArrayList<Integer>> tempList = getCombs(subList, k - 1);
		for (ArrayList<Integer> intArr : tempList) {
			intArr.add(originalList.get(originalList.size() - 1));
		}

		returnList.addAll(tempList);
		return returnList;
	}

	private static boolean checkTriple(ArrayList<Integer> triple) {
		return ((triple.get(0).intValue() < triple.get(1).intValue()) && (triple.get(1).intValue() < triple.get(2).intValue()) && (triple.get(1).intValue() % triple.get(0).intValue() == 0) && (triple.get(2).intValue() % triple.get(1).intValue() == 0));
	}

	public static void main(String[] args) {
		int[] x = new int[args.length];
		for (int i = 0; i < args.length; i++) {
			x[i] = Integer.parseInt(args[i]);
		}

		System.out.println(Answer.answer(x));
	}
}