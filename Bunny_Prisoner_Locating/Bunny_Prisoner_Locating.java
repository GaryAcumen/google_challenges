package com.google.challenges;
import java.util.*;

public class Answer {
    public static String answer(int x, int y) {
        int cellId = 1 + ((y+y+x)*(x-1)/2) + (y * (y-1)/2);
        return String.valueOf(cellId);
    }
}