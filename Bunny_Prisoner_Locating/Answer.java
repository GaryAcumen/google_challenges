// package com.google.challenges;
import java.lang.String;
import java.lang.Integer;
import java.math.BigInteger;

public class Answer {
	public static String answer(int x, int y) {
		BigInteger total = BigInteger.ZERO;

		for (int i = 1; i <= x; i++) {
			total = total.add(new BigInteger(String.valueOf(i)));
		}

		for (int j = 1; j < y; j++) {
			total = total.add(new BigInteger(String.valueOf(x)));
			total = total.add(new BigInteger(String.valueOf(j)));
			total = total.subtract(BigInteger.ONE);
		}

		return String.valueOf(total);
	}

	public static void main(String[] args) {
		int x = Integer.parseInt(args[0]);
		int y = Integer.parseInt(args[1]);

		System.out.println(Answer.answer(x, y));
	}
}