//package com.google.challenges; 
import java.lang.StringBuffer;
import java.lang.Integer;


public class Answer {
	public static String answer(int n) {
		String primes = generatePrimeString(n);
		return primes.substring(n, n+5);
	}

	public static String generatePrimeString(int n) {
		StringBuffer primeString = new StringBuffer();
		int index = 2;
		while (primeString.length() < n + 5) {
			boolean isPrime = true;
			if (index > 2 && index % 2 == 0) {
				isPrime = false;
			}
			for (int j = 3; j*j <= index; j+=2) {
				if (index % j == 0) {
					isPrime = false;
				}
			}
			if (isPrime) {
				primeString.append(String.valueOf(index));
			}
			index++;
		}
		return primeString.toString();
	}

	public static void main(String[] args) {
		System.out.println(Answer.answer(Integer.parseInt(args[0])));
	}
}