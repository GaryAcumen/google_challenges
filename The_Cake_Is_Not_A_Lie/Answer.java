// package com.google.challenges;

public class Answer {
	public static int answer(String s) {
		int pieceSize = 1;
		for (int i = 0; i < s.length(); i++) {
			if (isEvenSplit(s, pieceSize)) {
				return s.length()/pieceSize;
			} else {
				pieceSize++;
			}
		}
		return 1;
	}

	public static boolean isEvenSplit(String s, int pieceSize) {

		if (s.length() % pieceSize != 0) {
			return false;
		}

		for (int i = 0; i+pieceSize < s.length(); i += pieceSize) {
			if (!s.substring(i, i + pieceSize).equals(s.substring(i + pieceSize, i + 2 * pieceSize))) {
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		System.out.println(Answer.answer(args[0]));
	}
}