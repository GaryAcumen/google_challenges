package com.google.challenges; 
import java.util.*;

public class Answer {

	public static List<Integer> combinations;

	public static int answer(int[] l) { 
		combinations = new ArrayList<Integer>();
		possibleCombinations(l,0);
		int largestInt = 0;
		for (Integer x : combinations) {
			if (x > largestInt) {
				largestInt = x;
			}
		}
		return largestInt;
	}

	public static void possibleCombinations(int[] a, int k) {
		if (k == a.length) {
			String wholeInt = "";
			for (int i = 0; i < a.length; i++) {
				wholeInt += String.valueOf(a[i]);
			}
			Integer x = Integer.valueOf(wholeInt);
			if (x % 3 == 0) {
				combinations.add(x);
			}
		} else {
			for (int i = k; i < a.length; i++) {
				int temp = a[k];
				a[k] = a[i];
				a[i] = temp;

				possibleCombinations(a, k + 1);

				temp = a[k];
				a[k] = a[i];
				a[i] = temp;
			}
		}
	}

}