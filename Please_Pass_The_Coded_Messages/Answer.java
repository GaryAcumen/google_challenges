// package com.google.challenges;
import java.util.*;
import java.lang.*;

public class Answer {

	public static HashSet<Integer> allCombz = new HashSet<Integer>();

	public static int answer(int[] l) {
		ArrayList<Integer> workingArr = new ArrayList<Integer>();
		for (int i : l) {
			workingArr.add(i);
		}

		Collections.sort(workingArr);
		Collections.reverse(workingArr);

		makeCombz(makeString(workingArr), new StringBuilder(), 0);

		ArrayList<Integer> uniqueCombz = new ArrayList<Integer>(allCombz);
		Collections.sort(uniqueCombz);
		Collections.reverse(uniqueCombz);

		for (Integer s : uniqueCombz) {
			System.out.println(s.toString());
		}

		if (uniqueCombz.size() != 0) {
			return uniqueCombz.get(0);
		}

		return 0;

	}

	public static void makeCombz(String inputString, StringBuilder outputString, int index) {
		for (int i = index; i < inputString.length(); i++) {
			outputString.append(inputString.charAt(i));
			if(getSum(outputString.toString()) % 3 == 0) {
				allCombz.add(new Integer(outputString.toString()));
			}

			if (i < inputString.length()) {
				makeCombz(inputString, outputString, i + 1);
			}

			outputString.setLength(outputString.length() - 1);
		}
	}

	public static String makeString(ArrayList<Integer> l) {
		String s = "";
		for (Integer i : l) {
			s = s + i.toString();
		}
		return s;
	}

	public static int getSum(String str) {
		int sum = 0;
		for (int i = 0; i < str.length(); i++) {
			sum += (int)str.charAt(i);
		}
		return sum;
	}

	public static void main(String[] args) {
		int[] intArr = new int[args.length];
		for (int i = 0; i < intArr.length; i++) {
			intArr[i] = Integer.parseInt(args[i]);
		}

		System.out.println(Answer.answer(intArr));
	}
}