package com.google.challenges;
import java.util.*;

public class Answer {
    
    public static String[] answer(String[] l) {
        String[] sortedArray = processNumber(l, true);
        Arrays.sort(sortedArray);
        String[] finalArray = processNumber(sortedArray, false);
        return finalArray;
    }
    
    public static String[] processNumber(String[] strArray, Boolean padNum) {
        String[] formattedArray = new String[strArray.length];
        int i = 0;
        for (String str : strArray) {
            String[] parts = str.split("\\.");
            String rebuiltStr = "";
            for (String s : parts) {
                if (padNum) {
                    rebuiltStr = zeroPadElements(s, rebuiltStr);
                } else {
                    rebuiltStr = removeZeroPad(s, rebuiltStr);
                }
            }
            formattedArray[i] = rebuiltStr;
            i++;
        }
        return formattedArray;
    }
    
    public static String zeroPadElements(String s, String currStr) {
        String zeroPadStr = "";
        if (s.length() == 1) {
            zeroPadStr = "0" + s;
        } else {
            zeroPadStr = s;
        }
        return rebuildString(currStr, zeroPadStr);
    }
    
    public static String removeZeroPad(String s, String currStr) {
        String revertedStr = "";
        if (s.startsWith("0")) {
            revertedStr = s.substring(1);
        } else {
            revertedStr = s;
        }
        return rebuildString(currStr, revertedStr);
    }
    
    public static String rebuildString(String currStr, String alteredStr) {
        if (currStr == "") {
            currStr = alteredStr;
        } else {
            currStr += "." + alteredStr;
        }
        return currStr;
    }
}
