package com.google.challenges;
import java.util.*;
import java.lang.*;

public class Answer {   
	public static String answer(String plaintext) { 
		String capitalStart = "000001";
		String whiteSpace = "000000";
		Map<String, String> brailleMap = createBrailleMap();
		String brailleStr = "";
		
		for (int i=0; i<plaintext.length(); i++) {
			char c = plaintext.charAt(i);
			if (Character.isUpperCase(c)) {
				brailleStr += capitalStart;
				c = Character.toLowerCase(c);
				brailleStr += brailleMap.get(Character.toString(c));
			} else {
				if (Character.isSpaceChar(c)) {
					brailleStr += whiteSpace;
				} else {
					brailleStr += brailleMap.get(Character.toString(c));
				}
			}
		}
		
		return brailleStr;
	}
	
	public static Map<String, String> createBrailleMap() {
		Map<String, String> brailleMap = new HashMap<String,String>();
		brailleMap.put("a", "100000");
		brailleMap.put("b", "110000");
		brailleMap.put("c", "100100");
		brailleMap.put("d", "100110");
		brailleMap.put("e", "100010");
		brailleMap.put("f", "110100");
		brailleMap.put("g", "110110");
		brailleMap.put("h", "110010");
		brailleMap.put("i", "010100");
		brailleMap.put("j", "010110");
		brailleMap.put("k", "101000");
		brailleMap.put("l", "111000");
		brailleMap.put("m", "101100");
		brailleMap.put("n", "101110");
		brailleMap.put("o", "101010");
		brailleMap.put("p", "111100");
		brailleMap.put("q", "111110");
		brailleMap.put("r", "111010");
		brailleMap.put("s", "011100");
		brailleMap.put("t", "011110");
		brailleMap.put("u", "101001");
		brailleMap.put("v", "111001");
		brailleMap.put("w", "010111");
		brailleMap.put("x", "101101");
		brailleMap.put("y", "101111");
		brailleMap.put("z", "101011");
		return brailleMap;
	}
}