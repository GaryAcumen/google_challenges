// package com.google.challenges;
import java.util.Arrays;

public class Answer {
	public static int answer(int[] x, int[] y) {
		int[] longer;
		int[] shorter;

		if (x.length > y.length) {
			longer = x;
			shorter = y;
		} else {
			longer = y;
			shorter = x;
		}

		Arrays.sort(longer);
		Arrays.sort(shorter);

		for (int i = 0; i < shorter.length; i++) {
			if (shorter[i] != longer[i]) {
				return longer[i];
			}
		}

		return longer[longer.length-1];
	}

	public static void main(String[] args) {
		//test case
		int[] x = {14, 27, 1, 4, 2, 50, 3, 1};
		int[] y = {2, 4, -4, 3, 1, 1, 14, 27, 50};
		System.out.println(Answer.answer(x, y));
	}
}