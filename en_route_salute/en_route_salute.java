package com.google.challenges;
import java.util.*;

public class Answer {
    public static int answer(String s) {
        String str = s.replaceAll("-","");
        int count = 0;
        String[] sArray = str.split("");
        for (int i=0;i<sArray.length; i++) {
            if (sArray[i].equals(">")) {
                for (int x=i+1; x<sArray.length; x++) {
                    if (sArray[x].equals("<")) {
                        count = count+2;
                    }
                }
            }
        }
        return count;
    }
}