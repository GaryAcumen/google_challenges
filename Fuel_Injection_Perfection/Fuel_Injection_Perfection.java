import java.util.*;
import java.lang.*;
import java.math.*;

public class Answer {
	public static int answer(String n) {
		if (n.equals("0")) {
			return 1;
		}
		int count = 0;
		String s1 = "1";
		String s2 = "2";
		String s3 = "3";

		BigInteger bInt = new BigInteger(n);
		BigInteger divisor = new BigInteger(s2);
		BigInteger modInt = new BigInteger(s1);
		BigInteger rule3 = new BigInteger(s3);

		BigInteger bi = bInt;
		while (!bi.equals(modInt)) {

			BigInteger y = bi.mod(divisor);
			if (y.equals(modInt)) {
				BigInteger x = bi.add(modInt);
				x = x.divide(divisor);
				if (bi.equals(rule3)) {
					bi = bi.subtract(modInt);
				} else if (x.mod(divisor).equals(modInt)) {
					bi = bi.subtract(modInt);
				} else {
					bi = bi.add(modInt);
				}

				count++;
			}
			bi = bi.divide(divisor);
			count++;
		}
		return count;
	}

}