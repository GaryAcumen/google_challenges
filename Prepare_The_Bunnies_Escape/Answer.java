import java.math.*;
import java.util.*;
import java.lang.*;

public class Answer {

	public static Stack<Node> path = new Stack<Node>();
	public static HashSet<String> onPath = new HashSet<String>();
	public static Stack<Node> shortestPath = new Stack<Node>();

	public static int answer(int[][] maze) {
		Answer test = new Answer();
		Graph g = new Graph(maze);
		Node a = new Node(0, 0, 0);
		Node b = new Node(maze[0].length - 1, maze.length - 1, 0);
		enumerate(g, a, b);
		return shortestPath.size();
	}

	public static void enumerate(Graph g, Node a, Node b) {
		path.push(a);
		onPath.add(a.name);

			if (a.equals(b)) {
				checkPath(path);
			} else {
				if (g.adjMap.get(a.name) != null) {
					for (Node n : g.adjMap.get(a.name)) {
						if(!onPath.contains(n.name)) {
							enumerate(g, n, b);
						}
					}
				}
			}

		path.pop();
		onPath.remove(a.name);
	}

	public static void checkPath(Stack<Node> p) {
		int weight = 0;
		for (Node n : p) {
			weight += n.weight;
		}

		if (weight < 2) {
			if (shortestPath.size() == 0) {
				shortestPath.addAll(p);
			} else if (p.size() <= shortestPath.size()) {
				shortestPath.clear();
				shortestPath.addAll(p);
			}
		}
	}

	public static class Graph {
		//adjacency map
		public HashMap<String, HashSet<Node>> adjMap = new HashMap<String, HashSet<Node>>();
		public int numberOfEdges = 0;

		public Graph(int[][] maze) {
			for (int i = 0; i < maze[0].length; i++) {
				for (int j = 0; j < maze.length; j++) {
					Node one = new Node(i, j, maze[i][j]);
					if (i-1 >= 0) {
						Node left = new Node(i-1, j, maze[i-1][j]);
						addEdge(one, left);
					}
					if (i+1 < maze[0].length) {
						Node right = new Node(i+1, j, maze[i+1][j]);
						addEdge(one, right);
					}
					if (j+1 < maze.length) {
						Node up = new Node(i, j+1, maze[i][j+1]);
						addEdge(one, up);
					}
					if (j-1 >= 0) {
						Node down = new Node(i, j-1, maze[i][j-1]);
						addEdge(one, down);
					}
				}
			}
		}

		public void addEdge(Node one, Node two) {
			numberOfEdges++;
			if (adjMap.get(one.name) == null) {
				HashSet<Node> temp = new HashSet<Node>();
				temp.add(two);
				adjMap.put(one.name, temp);
			} else {
				adjMap.get(one.name).add(two);
			}
		}

	}

	public static class Node {
		public String name;
		public int x;
		public int y;
		public int weight;

		public Node(int xcoord, int ycoord, int w) {
			x = xcoord;
			y = ycoord;
			weight = w;
			name = String.valueOf(x) + String.valueOf(y);
		}

		@Override
		public boolean equals(Object other) {
			Node obj = (Node) other;
			if (this.name.equals(obj.name)) {
				return true;
			} else {
				return false;
			}
		}
	}

	public static void main(String[] args) {
		int[][] arrays = {{0, 1, 1, 0}, {0, 0, 0, 1}, {1, 1, 0, 0}, {1, 1, 1, 0}};
		// int[][] arrays = {{0, 0}, {0, 0}};
		// int[][] arrays = {{0, 1, 1}, {0, 0, 0}, {1, 1, 0}};
		System.out.println(Answer.answer(arrays));

	}
}