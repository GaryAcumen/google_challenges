package com.google.challenges; 

public class Answer {
	public static int locate(int head, int target, int under) {

		under /= 2;
		int right = head - 1;			// right = head - 1
		int left = head - 1 - under--; // left = head - 1 - under/2

		if (right == target || left == target)
			return head;
		else {
			if (target <= left)
				return locate(left,target,under);
			else
				return locate(right,target,under);
		}
	}

	public static int[] answer(int h, int[] q) {

		int head = (int)Math.pow(2,h) - 1;
		int[] result = new int[q.length];

		for (int i = 0 ; i < q.length ; i++) {
			if (q[i] < head && q[i] >=1)
				result[i] = locate(head,q[i],head-1);
			else 
				result[i] = -1;
		}

		return result;
	}
}