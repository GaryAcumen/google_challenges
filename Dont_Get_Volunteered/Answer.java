// package com.google.challenges;
import java.lang.*;
import java.util.*;

public class Answer {
	public static int totalMoves = 99;
	public static final int[] nextMoves = {17, -17, 15, -15, 10, -10, 6, -6};
	public static int answer(int src, int dest) {
		makeMoves(src, dest, 0, new HashSet<String>());
		return totalMoves;
	}

	public static void makeMoves(int src, int dest, int currentMoves, HashSet<String> visitedSpaces) {
		visitedSpaces.add(String.valueOf(src));
		if (src == dest) {
			if (currentMoves < totalMoves) {
				totalMoves = currentMoves;
			}
		}
		else {
			if (currentMoves < totalMoves) {
				if (!visitedSpaces.contains(String.valueOf(src + 17)) && src + 17 < 64 && src % 8 != 7) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src + 17, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src - 17)) && src - 17 >= 0 && src % 8 != 0) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src - 17, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src + 15)) && src + 15 < 64 && src % 8 != 0) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src + 15, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src - 15)) && src - 15 >= 0 && src % 8 != 7) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src - 15, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src + 10)) && src + 10 < 64 && src % 8 != 7 && src % 8 != 6) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src + 10, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src - 10)) && src - 10 >= 0 && src % 8 != 0 && src % 8 != 1) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src - 10, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src + 6)) && src + 6 < 64 && src % 8 != 0 && src % 8 != 1) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src + 6, dest, currentMoves + 1, copy);
				}
				if (!visitedSpaces.contains(String.valueOf(src - 6)) && src - 6 >= 0 && src % 8 != 7 && src % 8 != 6) {
					HashSet<String> copy = new HashSet<String>(visitedSpaces);
					makeMoves(src - 6, dest, currentMoves + 1, copy);
				}
			}
		}
	}

	public static void main(String[] args) {
		int src = Integer.parseInt(args[0]);
		int dest = Integer.parseInt(args[1]);
		System.out.println(Answer.answer(src, dest));
	}

}