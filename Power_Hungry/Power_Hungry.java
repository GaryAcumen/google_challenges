package com.google.challenges;
import java.util.*;
import java.lang.*;
import java.math.BigInteger;

public class Answer {
	public static String answer(int[] xs) {
		String s1 = "1";
		String s0 = "0";
		BigInteger zero = new BigInteger(s0);
		BigInteger one = new BigInteger(s1);
		
		List<Integer> mathInts = new ArrayList<Integer>();
		int nCount = 0;
		int zeroCount = 0;
		for (int i : xs) {
			if (i < 0) {
				nCount++;
			}
			if (i != 0) {
				mathInts.add(i);
			} else {
				zeroCount++;
			}
		}
		
		if (mathInts.size() == 1) {
			for (int i : mathInts) {
				if (i < 0 && zeroCount > 0) {
					return String.valueOf(i*0);
				}
				return String.valueOf(i);
			}
		}

		int bInt = checkForBadInt(mathInts, nCount);
		
		BigInteger total = findTotal(mathInts, bInt, zero);

		return total.toString();
	}
	
	public static BigInteger findTotal(List<Integer> mathInts, int bInt, BigInteger zero) {
		BigInteger total = new BigInteger("0");
		for (int i : mathInts) {
			if (i != bInt) {
				BigInteger temp = new BigInteger(String.valueOf(i));
				if (total.equals(zero)) {
					total = total.add(temp);
				} else {
					total = total.multiply(temp);
				}
				
			} else {
				bInt = 0;
			}
		}
		return total;
	}
	
	public static int checkForBadInt(List<Integer> mathInts, int nCount) {
		int bInt = 0;
		if (nCount%2 != 0) {
			//get rid of highest negative value
			for (Integer i : mathInts) {
				if (i < 0 && (bInt == 0 || i > bInt)) {
					bInt = i;
				}
			}
			//System.out.println(bInt);
		}
		return bInt;
	}
}