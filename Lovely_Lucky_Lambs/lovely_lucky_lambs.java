package com.google.challenges;
import java.util.*;
import java.lang.Double;
import java.lang.Integer;

public class Answer {
	public static int answer(int total_lambs) {
		return calculateStringiest(total_lambs) - calculateGenerous(total_lambs);
	}
	
	public static int calculateGenerous(int total_lambs) {
	    if (total_lambs <=1) {
            return 0;
        }
        //Double y = Math.log10(total_lambs)/Math.log10(2);
        //Integer x = y.intValue();
        int henchmen = 1;
        int allowance = 1;
        int totalLeft = total_lambs - allowance;
        
        while (totalLeft > 0) {
            allowance *= 2;
            totalLeft -= allowance;
            if (totalLeft >= 0) {
                henchmen++;
            }
        }
        return henchmen;
	}
	
	public static int calculateStringiest(int total_lambs) {
	    if (total_lambs <= 1) {
            return 0;
        }
        
		int henchmen = 1;
		int totalLeft = total_lambs - 0;
		int n1 = 0;
		int n2 = 0;
		int n3 = 1;

		while (totalLeft > 0) {
		    n1 = n2;
		    n2 = n3;
		    n3 = n1+n2;
		    totalLeft -= n3;
		    if (totalLeft >= 0) {
		        henchmen++;
		    }
		}
		return henchmen;
	}
}