#!/bin/python

import sys
import operator

def answer(start, length):
    checksum = reduce(operator.xor, xrange(start, start+length), 0)
    i = 0
    while length > 0:
        start += length + i
        length -= 1
        i += 1
        checksum ^= reduce(operator.xor, xrange(start, start+length), 0)
    return checksum

if __name__ == "__main__":
    start = int(raw_input().strip())
    length = int(raw_input().strip())

    print(answer(start, length))